## Installation

Run the next commands in console

- docker-compose up -d

- docker-compose exec webserver bash

- composer install

- mysql -hdatabase -uroot -psample playmistertest < database/schema.sql

- mysql -hdatabase -uroot -psample playmistertest < database/data.sql



## Unit Tests

- To run the unit tests use: ./vendor/bin/phpunit tests

## Notes

- Each users starts with a budget of 1000 $us
- The "Market" table starts empty, the same is populated whenis needed using the market configuration, you can clean the tabke in order to re-populate the same with another characters an prices
