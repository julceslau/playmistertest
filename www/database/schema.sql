CREATE TABLE `USERS` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`username` varchar(100) DEFAULT NULL,
	`first_name` varchar(150) DEFAULT NULL,
	`last_name` varchar(150) DEFAULT NULL,
	`email` varchar(150) DEFAULT NULL,
	`budget` int(11) DEFAULT 0,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `SPORTS` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(100) DEFAULT NULL,
	`minimum` tinyint(4) DEFAULT 8,
	`maximum` tinyint(4) DEFAULT 17,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TEAMS` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
    `sport_id` int(11) DEFAULT NULL,
	`name` varchar(150) DEFAULT NULL,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MEMBERS` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `team_id` int(11) DEFAULT NULL,
    `character_id` int(11) DEFAULT NULL,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CHARACTERS` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `sport_id` int(11) DEFAULT NULL,
	`name` varchar(100) DEFAULT NULL,
	`photo` varchar(150) DEFAULT NULL,
	`strength` tinyint(4) DEFAULT 0,
	`agility` tinyint(4) DEFAULT 0,
	`luck` tinyint(4) DEFAULT 0,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MARKET_CONFIGURATION` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`characters_number` tinyint(4) DEFAULT 5,
	`minimum_price` tinyint(4) DEFAULT 0,
	`maximum_price` tinyint(4) DEFAULT 0,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MARKET` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `character_id` int(11) DEFAULT NULL,
    `price` tinyint(4) DEFAULT 0,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`deleted_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
