<?php

// Include Database constants
require_once 'config/database.php';

// Include vendors
require_once 'vendor/autoload.php';

// Namespaces to use
use PlayMisterTest\Models\Database;

// Initialize database connection
new Database();
