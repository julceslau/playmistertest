<?php
// Initialize app
require_once 'init.php';

// Namespaces to use
use PlayMisterTest\Controllers\Controller;

// Get parameters from request
$module = !empty($_REQUEST['m']) ? $_REQUEST['m'] : 'users';
$action = !empty($_REQUEST['a']) ? $_REQUEST['a'] : 'index';
$params = [
    'userId' => !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : null,
    'sportId' => !empty($_REQUEST['sport_id']) ? $_REQUEST['sport_id'] : null,
    'name' => !empty($_REQUEST['name']) ? $_REQUEST['name'] : null,
    'teamId' => !empty($_REQUEST['team_id']) ? $_REQUEST['team_id'] : null,
    'characterId' => !empty($_REQUEST['character_id']) ? $_REQUEST['character_id'] : null
];

// Instance controller class
$controller = new Controller($module, $params);

// Render/Execute action requested
$controller->$action();
