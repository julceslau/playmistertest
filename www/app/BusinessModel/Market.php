<?php

namespace PlayMisterTest\BusinessModel;

use Illuminate\Database\Capsule\Manager as DB;
use PlayMisterTest\Models\Market as MarketModel;
use PlayMisterTest\Models\MarketConfiguration;
use PlayMisterTest\Models\Member;
use PlayMisterTest\Models\User;

class Market
{
    private $configuration = null;

    public function __construct()
    {
        $this->configuration = MarketConfiguration::query()->first()->toArray();
    }

    public function isEmpty()
    {
        return MarketModel::query()->count() === 0;
    }

    public function populate()
    {
        $query = "(SELECT * FROM CHARACTERS WHERE sport_id = 1 ORDER BY RAND() LIMIT {$this->configuration['characters_number']})";
        $query .= " UNION (SELECT * FROM CHARACTERS WHERE sport_id = 2 ORDER BY RAND() LIMIT {$this->configuration['characters_number']})";
        $query .= " UNION (SELECT * FROM CHARACTERS WHERE sport_id = 3 ORDER BY RAND() LIMIT {$this->configuration['characters_number']})";
        $characters = DB::connection()->select(DB::raw($query));
        foreach ($characters as $character) {
            MarketModel::create([
                'character_id' => $character->id,
                'price' => rand($this->configuration['minimum_price'], $this->configuration['maximum_price'])
            ]);
        }
    }

    public function availableCharacters($team)
    {
         // TODO: Filter characters already assigned
         $query = "SELECT * FROM MARKET LEFT JOIN CHARACTERS ON (MARKET.character_id = CHARACTERS.id) WHERE CHARACTERS.sport_id = {$team->sport_id}";
         return DB::connection()->select(DB::raw($query));
    }

    public function buy($userId, $teamId, $characterId)
    {
        // Get current user
        $user = User::query()->where('id', '=', $userId)->first();

        // Get character from market
        $character = MarketModel::query()->where('character_id', '=', $characterId)->first();

        // Calculate budget, adding 20%
        $budget = $user->budget * 1.2;

    	// Check character price
        if ($character->price > $budget) {
        	throw new Exception('Budget not enough.');
        }

    	// Buy character
        Member::create([
            'team_id' => $teamId,
            'character_id' => $characterId
        ]);

        // Pay
        User::query()->where('id', '=', $userId)->update(['budget' => $user->budget - $character->price]);
    }
}
