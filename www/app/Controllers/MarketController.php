<?php

namespace PlayMisterTest\Controllers;

use PlayMisterTest\BusinessModel\Market;
use PlayMisterTest\Models\Character;
use PlayMisterTest\Models\Member;
use PlayMisterTest\Models\Team;
use PlayMisterTest\Models\User;

class MarketController
{
    private $userId = null;
    private $teamId = null;
    private $sportId = null;
    private $market = null;

    public function __construct(Market $market)
    {
        $this->market = $market;
    }

    /**
     * Set the user id
     *
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Set the team id
     *
     * @param int $teamId
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    /**
     * Set the character id
     *
     * @param int $characterId
     */
    public function setCharacterId($characterId)
    {
        $this->characterId = $characterId;
    }

    /**
     * Display the available characters in the market
     */
    public function index()
    {
        // If the market is empty, populate according to the Market configuration
        if ($this->market->isEmpty()) {
            $this->market->populate();
        }

        // Get current user
        $user = User::query()->where('id', '=', $this->userId)->first();

        // Get current team
        $team = Team::query()->where('id', '=', $this->teamId)->first();

        // Get available characters to buy
        $characters = $this->market->availableCharacters($team);

        // Display members from a team
        echo "<h2>Available characters to buy (Budget {$user->budget} \$us)</h2>";
        foreach ($characters as $character) {
            echo "<a href=\"index.php?m=market&a=buy&user_id={$user->id}&team_id={$team->id}&character_id={$character->id}\"><button>Buy</button></a> {$character->name} ({$character->price} \$us)<br />";
        }
        echo "<br /><a href=\"index.php?m=members&user_id={$user->id}&team_id={$team->id}\"><button>Back</button></a>";
    }

    public function buy()
    {
        // Buy character and assign to the corresponding team
        $this->market->buy($this->userId, $this->teamId, $this->characterId);

        // Redirect
        header("Location: index.php?m=members&user_id={$this->userId}&team_id={$this->teamId}");
    }
}
