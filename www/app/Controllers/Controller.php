<?php

namespace PlayMisterTest\Controllers;

use PlayMisterTest\BusinessModel\Market;

class Controller
{
    private $controller = null;

    /**
     * Class constructor
     *
     * @param string $module
     * @param array $params
     */
    public function __construct($module = '', array $params = []) {
        // Instance
        switch ($module) {
            case 'market':
                $controller = new MarketController(new Market());
                $controller->setUserId($params['userId']);
                $controller->setTeamId($params['teamId']);
                $controller->setCharacterId($params['characterId']);
                break;
            case 'members':
                $controller = new MemberController();
                $controller->setUserId($params['userId']);
                $controller->setTeamId($params['teamId']);
                $controller->setCharacterId($params['characterId']);
                break;
            case 'teams':
                $controller = new TeamController();
                $controller->setUserId($params['userId']);
                $controller->setSportId($params['sportId']);
                $controller->setName($params['name']);
                break;
            case 'users':
            default:
                $controller = new UserController();
                break;
        }

        // Assign controller instance
        $this->controller = $controller;
    }

    /**
     * Get controller
     *
     * @return object $controller
     */
    public function getController() {
        return $this->controller;
    }

    // Display/Render module
    public function index() {
        $this->controller->index();
    }

    // Display form
    public function form() {
        $this->controller->form();
    }

    // Create action
    public function create() {
        $this->controller->create();
    }

    // Buy action
    public function buy() {
        $this->controller->buy();
    }
}
