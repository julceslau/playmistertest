<?php

namespace PlayMisterTest\Controllers;

use PlayMisterTest\Models\Sport;
use PlayMisterTest\Models\Team;
use PlayMisterTest\Models\User;

class TeamController
{
    private $userId = null;
    private $sportId = null;
    private $name = null;

    /**
     * Set the user id
     *
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Set the sport id
     *
     * @param int $sportId
     */
    public function setSportId($sportId)
    {
        $this->sportId = $sportId;
    }

    /**
     * Set the sport name
     *
     * @param int $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Display the available teams for an user
     */
    public function index()
    {
        // Get teams from the database
        $teams = Team::query()->where('user_id', '=', $this->userId)->get();

        // Get current user
        $user = User::query()->where('id', '=', $this->userId)->first();

        // Display teams for an user
        echo "<h2>Available Teams for user: {$user->username}</h2>";
        foreach ($teams as $team) {
            echo "- <a href=\"index.php?m=members&user_id={$user->id}&team_id={$team->id}\">{$team->name}</a><br />";
        }
        echo "<br /><a href=\"index.php\"><button>Home</button></a>";
        echo "<a href=\"index.php?m=teams&a=form&user_id={$user->id}\"><button>Create New Team</button></a>";
    }

    /**
     * Display the for to create a new team
     */
    public function form()
    {
        // Get current user
        $user = User::query()->where('id', '=', $this->userId)->first();

        // Get sports
        $sports = Sport::query()->get();

        // Render form
        echo "<h2>New Team for user: {$user->username}</h2>";
        echo "<form action=\"index.php?m=teams&a=create\" method=\"POST\">";
        echo "<input type=\"hidden\" name=\"user_id\" value=\"{$user->id}\" />";
        echo "<label>Sport:</label><br /><select name=\"sport_id\">";
        foreach ($sports as $sport) {
            echo "<option value=\"{$sport->id}\">{$sport->name}</option>";
        }
        echo "</select><br />";
        echo "<label>Name:</label><br /><input type=\"text\" name=\"name\" required=\"required\" /><br /><br />";
        echo "<input type=\"submit\" value=\"Submit\" /></form>";
    }

    /**
     * Create a team
     */
    public function create()
    {
        // Create new team
        Team::create([
            'user_id' => $this->userId,
            'sport_id' => $this->sportId,
            'name' => $this->name
        ]);

        // Redirect
        header("Location: index.php?m=teams&user_id={$this->userId}");
    }
}
