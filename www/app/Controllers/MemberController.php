<?php

namespace PlayMisterTest\Controllers;

use PlayMisterTest\Models\Character;
use PlayMisterTest\Models\Member;
use PlayMisterTest\Models\Team;
use PlayMisterTest\Models\User;

class MemberController
{
    private $userId = null;
    private $teamId = null;
    private $characterId = null;

    /**
     * Set the user id
     *
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Set the team id
     *
     * @param int $teamId
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    /**
     * Set the character id
     *
     * @param int $characterId
     */
    public function setCharacterId($characterId)
    {
        $this->characterId = $characterId;
    }

    /**
     * Display the available teams for an user
     */
    public function index()
    {
        // Get team members
        $members = Member::query()->select(['MEMBERS.character_id', 'CHARACTERS.name'])->join('CHARACTERS', 'MEMBERS.character_id', '=', 'CHARACTERS.id')->where('team_id', '=', $this->teamId)->get();

        // Get current team
        $team = Team::query()->where('id', '=', $this->teamId)->first();

        // Get current user
        $user = User::query()->where('id', '=', $this->userId)->first();

        // Display members from a team
        echo "<h2>Members characters from team: {$team->name}</h2>";
        foreach ($members as $member) {
            echo "- {$member->name}<br />";
        }
        echo "<br /><a href=\"index.php?m=teams&user_id={$user->id}\"><button>Back</button></a>";
        echo "<a href=\"index.php?m=market&user_id={$user->id}&team_id={$team->id}\"><button>Buy Character</button></a>";
    }
}
