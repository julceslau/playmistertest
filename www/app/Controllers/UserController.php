<?php

namespace PlayMisterTest\Controllers;

use PlayMisterTest\Models\User;

class UserController
{
    /**
     * Display the available users
     */
    public function index()
    {
    	// Get users from the database
    	$users = User::query()->get();

    	// Display users
    	echo "<h2>Available Users</h2>";
    	foreach ($users as $user) {
    		echo "- <a href=\"index.php?m=teams&user_id={$user->id}\">{$user->username}</a><br /><br />";
    	}
    }
}
