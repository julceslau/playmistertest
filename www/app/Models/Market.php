<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    protected $table = 'MARKET';
    protected $fillable = [
        'character_id',
        'price'
    ];
}
