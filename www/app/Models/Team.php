<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'TEAMS';
    protected $fillable = [
        'user_id',
        'sport_id',
        'name'
    ];
}
