<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'MEMBERS';
    protected $fillable = [
        'team_id',
        'character_id'
    ];
}
