<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class MarketConfiguration extends Model
{
    protected $table = 'MARKET_CONFIGURATION';
}
