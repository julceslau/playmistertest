<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = 'CHARACTERS';
}
