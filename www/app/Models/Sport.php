<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    protected $table = 'SPORTS';
}
