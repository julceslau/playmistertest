<?php

namespace PlayMisterTest\Models;

use Illuminate\Database\Capsule\Manager;

class Database {
    /**
     * Class constructor.
     */
    public function __construct() {
        // Instance Manager class
        $manager = new Manager();

        // Add a new connection
        $manager->addConnection([
            'driver' => DB_DRIVER,
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ]);
        $manager->setAsGlobal();

        // Boot Eloquent ORM
        $manager->bootEloquent();
    }
}
