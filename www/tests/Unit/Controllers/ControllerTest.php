<?php

namespace PlayMisterTest\Controllers;

use PHPUnit\Framework\TestCase;
use PlayMisterTest\Controllers\Controller;
use PlayMisterTest\Controllers\TeamController;
use PlayMisterTest\Controllers\UserController;

class ControllerTest extends TestCase
{
    public function __construct() {
        // Initialize app
        require_once 'init.php';
    }

    public function testControllerClassInstance()
    {
        // Test user controller instance
        $module = 'users';
        $controller = new Controller($module);
        $this->assertInstanceOf(UserController::class, $controller->getController());

        // Test team controller instance
        $module = 'teams';
        $params = [
            'userId' => 1,
            'sportId' => 1,
            'name' => null
        ];
        $controller = new Controller($module, $params);
        $this->assertInstanceOf(TeamController::class, $controller->getController());

        // Test default controller instance
        $module = 'inexistent';
        $controller = new Controller($module);
        $this->assertInstanceOf(UserController::class, $controller->getController());
    }

    public function testIndex()
    {
        // Instance controller instance
        $controller = new Controller();

        // Turn on output buffering
        ob_start();

        // Display content
        $controller->index();

        // Return the contents of the output buffer
        $content = ob_get_contents();

        // Clean the output buffer and turn off output buffering
        ob_end_clean();

        // Assert content
        $this->assertContains('index.php?m=', $content);
    }
}
