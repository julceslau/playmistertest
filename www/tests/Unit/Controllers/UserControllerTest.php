<?php

namespace PlayMisterTest\Controllers;

use PHPUnit\Framework\TestCase;
use PlayMisterTest\Controllers\UserController;

class UserControllerTest extends TestCase
{
    public function __construct() {
        // Initialize app
        require_once 'init.php';
    }

    public function testIndex()
    {
        // Instance user controller instance
        $userController = new UserController();

        // Turn on output buffering
        ob_start();

        // Display content
        $userController->index();

        // Return the contents of the output buffer
        $content = ob_get_contents();

        // Clean the output buffer and turn off output buffering
        ob_end_clean();

        // Assert content
        $this->assertContains('index.php?m=teams&user_id=', $content);
    }
}
