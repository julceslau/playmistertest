<?php

namespace PlayMisterTest\Controllers;

use PHPUnit\Framework\TestCase;
use PlayMisterTest\Controllers\TeamController;

class TeamControllerTest extends TestCase
{
    public function __construct() {
        // Initialize app
        require_once 'init.php';
    }

    public function testIndex()
    {
        // Instance team controller instance
        $teamController = new TeamController();
        $teamController->setUserId(1);

        // Turn on output buffering
        ob_start();

        // Display content
        $teamController->index();

        // Return the contents of the output buffer
        $content = ob_get_contents();

        // Clean the output buffer and turn off output buffering
        ob_end_clean();

        // Assert content
        $this->assertContains('index.php?m=members&user_id=', $content);
    }

    public function testForm()
    {
        // Instance team controller instance
        $teamController = new TeamController();
        $teamController->setUserId(1);

        // Turn on output buffering
        ob_start();

        // Display form
        $teamController->form();

        // Return the contents of the output buffer
        $content = ob_get_contents();

        // Clean the output buffer and turn off output buffering
        ob_end_clean();

        // Assert content
        $this->assertContains('<form action="index.php?m=teams&a=create" method="POST">', $content);
    }
}
